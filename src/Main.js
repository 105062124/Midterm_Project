import Component from  './Component.js';
import Navbar from './Navbar.js';
import Board from './Board.js';
import Router from './Router.js';

import './Main.css';

export default class Main extends Component {
    static getRootClass() {
        return 'body';
    }
    
    constructor(root) {
        super(root);

        if (Notification.permission === 'default' || Notification.permission === 'undefined') {
            Notification.requestPermission(function (permission) {
                if (permission === 'granted') {
                    // var notification = new Notification('Hi there!', notifyConfig);
                    console.log(`successed to notify`);
                }
                else {
                    console.log(`failed to notify`);
                }
            });
        }

        root.querySelector('#m_goBack').addEventListener('click', () => {
            window.history.back();
        });
        window.onscroll = () => {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.querySelector('#m_goTop').style.display = "block";
            } else {
                document.querySelector('#m_goTop').style.display = "none";
            }
        };
        root.querySelector('#m_goTop').addEventListener('click', () => {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        });

        this.m_navbar = new Navbar(root.querySelector(Navbar.getRootClass()));
        this.m_navbar.on('signUpBtnClick', this.handleSignUpBtnClick.bind(this));
        this.m_navbar.on('signInBtnClick', this.handleSignInBtnClick.bind(this));
        this.m_navbar.on('logOutBtnClick', this.handleLogOutBtnClick.bind(this));
        this.m_navbar.on('profileBtnClick', this.handleProfileBtnClick.bind(this));
        this.m_navbar.on('personalPageBtnClick', this.handlePersonalPageBtnClick.bind(this));
        
        this.m_board = new Board(root.querySelector(Board.getRootClass()));
        this.m_board.on('signup', (firer, mode) => {
            if(mode === 'back') {
                this.m_router.navigate('/category');
            }
            else if(mode === 'success') {
                this.m_router.navigate('/category');
            }
        });
        this.m_board.on('signin', (firer, mode) => {
            if(mode === 'back') {
                this.m_router.navigate('/category');
            }
            else if(mode === 'success') {
                this.m_router.navigate('/category');
            }
        });
        this.m_board.on('profile', (firer, mode) => {
            if(mode === 'back') {
                this.m_router.navigate('/category');
            }
        });
        this.m_board.on('subject', (firer, subjectName) => {
            this.m_router.navigate(`/category/${subjectName}`);
        });
        this.m_board.on('postClickFromBoard', (firer, subjectName, postId) => {
            this.m_router.navigate(`/category/${subjectName}/${postId}`);
        });
        this.m_board.on('personalPagePostClickFromBoard', (firer, subjectName, postId) => {
            this.m_router.navigate(`/category/${subjectName}/${postId}`);
        });
        this.m_board.on('subjectNotFoundFromBoard', (firer) => {
            this.m_router.navigate('/notfound');
        });
        this.m_board.on('postNotFoundFromBoard', (firer) => {
            this.m_router.navigate('/notfound');
        });
        // console.log(window.location.href.match(/\#!.*/)[0].substring(2));
        this.m_router = new Router(false, '/', this.m_board);
        if(window.location.href.match(/\#!.*/) == null) {
            this.m_router.navigate('/category');
        }
        else if(window.location.href.match(/\#!.*/)[0] != '#!/category') {
            this.m_router.navigate(`/${window.location.href.match(/\#!.*/)[0].substring(2)}`);
        }

        root.querySelector('#THF').addEventListener('click', () => {
            this.m_router.navigate('/category');
            // window.location.reload();
        });
    }

    handleSignUpBtnClick(firer) {
        this.m_router.navigate('/signup');
        this.root.querySelector('#m_navbar_collapse').classList.remove('show');
    }
    handleSignInBtnClick(firer) {
        this.m_router.navigate('/signin');
        this.root.querySelector('#m_navbar_collapse').classList.remove('show');
    }
    handleLogOutBtnClick(firer) {
        // window.location.reload();
        this.m_router.navigate('/category');
        this.root.querySelector('#m_navbar_collapse').classList.remove('show');
    }
    handleProfileBtnClick(firer, userProfileVal) {
        this.m_router.navigate(`/profile/${userProfileVal.uid}`);
        this.root.querySelector('#m_navbar_collapse').classList.remove('show');
    }
    handlePersonalPageBtnClick(firer, userProfileVal) {
        this.m_router.navigate(`/page/${userProfileVal.uid}`);
        this.root.querySelector('#m_navbar_collapse').classList.remove('show');
    }
}

var config = {
    apiKey: "AIzaSyA0KdBMYekhdrzn6vSO9N5W-5haBJ9swPQ",
    authDomain: "software-studio-mid.firebaseapp.com",
    databaseURL: "https://software-studio-mid.firebaseio.com",
    projectId: "software-studio-mid",
    storageBucket: "software-studio-mid.appspot.com",
    messagingSenderId: "495335816345"
};
firebase.initializeApp(config);

window.onload = function() {
    const body = document.querySelector('body');
    new Main(body);
};