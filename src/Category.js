import Component from  './Component.js';
import Subject from './Subject.js';

import './Category.css';

export default class Category extends Component {
    static getRootClass() {
        return '.m_category';
    }

    constructor(root) {
        super(root);

        setTimeout(() => {
            root.style.opacity = "1";
        }, 10);

        this.subjects = [];
        

        const children = root.querySelectorAll('.m_categorySubject');
        for(let child of children) {
            let subjectName = child.querySelector('.card-title').textContent.toLowerCase();
            let latestTitle = '';
            let latestDate = '';
            let latestAuthor = '';
            let latestLike = '';
            firebase.database().ref(`m_list/${subjectName}`).orderByChild('postDate').on('value', snapshot => {
                snapshot.forEach(childSnapshot => {
                    latestTitle = childSnapshot.val().postTitle;
                    latestDate = childSnapshot.val().postDate;
                    latestAuthor = childSnapshot.val().authorName;
                    latestLike = (childSnapshot.hasChild('like')) ? childSnapshot.child('like').numChildren() : 0;
                    console.log(latestLike);
                    // console.log(latestTitle, latestDate);
                });
                if(latestTitle !== '' && latestDate !== '' && latestAuthor !== '') {
                    root.querySelector(`#m_latest_${subjectName}_title`).textContent = `${latestTitle}`;
                    root.querySelector(`#m_latest_${subjectName}_date`).textContent = `${latestAuthor} @ ${latestDate}`;
                }
            });
            // console.log(subjectName, child);
            child.addEventListener('click', this.handleSubjectClick.bind(this));
            this.subjects.push(child);
        }
    }

    reset() {
        // do nothing
    }

    handleSubjectClick(evt) {
        const subjectName = evt.currentTarget.querySelector('.card-title').textContent;
        console.log(subjectName.toString().toLowerCase());
        this.fire('click', `${subjectName.toString().toLowerCase()}`);
    }
}